import { Component, OnInit } from '@angular/core';
import { MainServiceService } from '../main-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-appform',
  templateUrl: './appform.component.html',
  styleUrls: ['./appform.component.css'],
  providers: [MainServiceService]
})
export class AppformComponent implements OnInit {

  bulkShow : any ;
  input : any ;
  text : any ;
  content = [] ;
  temp : any[] = [] ;
  showDataTable : any ;
  dataTable : any[] = [] ;
  final_arguments : any[] = [] ;
  errorTable : any ;
  selectedAll: any;
  options = ["volunteer","organiser", "participant"];
  optionSelected: any;




  constructor(private router: Router,
               private mainService: MainServiceService ) { }

  ngOnInit() {
  	this.bulkShow = true ;
  }

  toggleForms() {
  	if(this.bulkShow){
  		this.bulkShow = false ;
  	}
  	else{
  		this.bulkShow = true ;
  		this.showDataTable = false ;
  	}
  }

  openFile(event) {
  	console.log('uploadfile');
    this.input = event.target;
    for (var index = 0; index < this.input.files.length; index++) {
        let reader = new FileReader();
        reader.onload = () => {
            // this 'text' is the content of the file
            this.text = reader.result;
            this.temp = this.text.split("\n")          
            for(var item in this.temp){
        		var elem = this.temp[item].split(','); 
        		var dict = {
        			"name":elem[0],
        			"email":elem[1],
        			"checked": false ,
        			"role": 'volunteer'
        		}
        		//console.log(dict);
        		this.dataTable.push(dict);

        	}
        }
        reader.readAsText(this.input.files[index]);
    };
  }

	showPreview(){
		this.showDataTable = true ;
		var check = this.dataTable[0] ;
		//console.log(check["name"]);
		if((check["name"]=="name" || check["name"]=="Name") && (check["email"]=="email" || check["email"]=="Email")) {
			this.errorTable = false ;
			console.log(this.dataTable);
		}
		else
		{
			this.errorTable = true ;
			console.log("invalid format");
		}	
	}

	selectAll() {
    for (var i = 0; i < this.dataTable.length; i++) {
      this.dataTable[i].checked = this.selectedAll;
    }
  	}
  	
  	checkIfAllSelected() {
    this.selectedAll = this.dataTable.every(function(item:any) {
        return item.selected == true;
      })
  	}

	onChangeRole(event,item) {  // event will give you full breif of action
    item["role"] = event.target.value;
    console.log(item);
  	}

  	isChecked(element, index, array) { 
   		return (element["checked"] == true); 
  	}

  	submitData(){
  		//for (var i = 0; i < this.dataTable.length; i++) {
     	//	 console.log(this.dataTable[i])

     	this.final_arguments = this.dataTable.filter(this.isChecked); 
     	//this.mainService.submit_data(this.final_arguments).subscribe(
       	//(response) => {
     
       	//})
       	console.log(this.final_arguments);
    }
  	

}
