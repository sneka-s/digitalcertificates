import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent} from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AppformComponent } from './appform/appform.component';
import {RouterModule,Routes} from '@angular/router';

const routes: Routes = [
  { path :'', redirectTo:'/login',pathMatch:'full'},
  { path :'login',component:LoginComponent },
  { path :'appform',component: AppformComponent}
];

@NgModule({
  imports: [
    CommonModule,RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
